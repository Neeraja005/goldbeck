import React, { Component } from 'react';
import logo from './logo.svg';

import './App.css';
import Nameform from './step1'
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">

          <img src={logo} className="App-logo" alt="logo" />
       
          <h2>GoldBeck GmbH</h2>

        </header>

        
        <Nameform/>

      </div>
    );
  }
}

export default App;
