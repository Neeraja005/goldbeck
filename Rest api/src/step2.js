import React, {Component} from 'react';
import axios from 'axios';

class Step2 extends Component {
    constructor() {
      super();
      this.handleSubmit = this.handleSubmit.bind(this);
      this.changerandom = this.changerandom.bind(this);
      this.externalAPIcall = this.externalAPIcall.bind(this);
      this.externalPOSTcall = this.externalPOSTcall.bind(this);
    }
  
    handleSubmit(event) {
      event.preventDefault();

      const form = event.target;
      const data = new FormData(form);        
   //Function to stringify form data   
  function stringifyFormData(fd) {
    const data = {};
      for (let key of fd.keys()) {
        data[key] = fd.get(key);
    }
    return JSON.stringify(data, null, 2);
  }

  //Parsing stringified FormData
  var jsondata = JSON.parse(stringifyFormData(data))

      // console.log(jsondata.deviceID);
      this.externalAPIcall(jsondata.URL, jsondata.deviceID)
      this.changerandom(event)
    }

    changerandom(e){
      e.target.reset();
    }  
  
    externalAPIcall(url,deviceID){
      const username = "Neeraja"
      const password = "f7e5b1g4w4"
      const apikey = "264b63333e951a6c327d627003f6a828"
      var b = new Buffer(username + ':' + password);
      var encodeAuth = b.toString('base64')
      var urlstring = url //Assign the URL attribute here
      var loggerID = urlstring.slice(50, 56); //capturing substring from main string
      var pullID = loggerID  //merging dataloger ID with device ID

      fetch(urlstring, {
          headers: {
              'Authorization': 'Basic' + encodeAuth,
              'X-API-KEY' : apikey
          }
      })
      .then((response)=>{
        return response.json()
      })
      .then((responseJson) => {
      //  console.log(responseJson.data[pullID].P_AC)
        this.externalPOSTcall(responseJson.data[pullID].T)
        // this.setState({
        //   data : responseJson.data["Id101409.1"].P_AC
        //   })
        })
  }

  externalPOSTcall(data, loggerID, deviceID){
    var dataloggerID;
    if(loggerID === "140645"){
      dataloggerID = 1;
    }
    if(loggerID === "Id101445"){
      dataloggerID = 2;
    }
    if(loggerID === "Id101408"){
      dataloggerID = 3;
    }
    if(loggerID === "Id101403"){
      dataloggerID = 4;
    }
    if(loggerID === "Id101338"){
      dataloggerID = 5;
    }
    if(loggerID === "Id101337"){
      dataloggerID = 6;
    }
    var url = "http://localhost:3003/P_AC1"
    var url1 = "http://localhost:3000/P_AC20"
    var url2 = "http://localhost:3000/P_AC29"
    var url3 = "http://localhost:3000/P_AC31"
  
    var urlstring = url + deviceID 
    console.log(url)
    // var tempdata=data
    // for(var i=0;i<tempdata.length;i++){
    //   tempdata[i].loggerID=dataloggerID;
    // }
    // console.log(tempdata);
    axios({
      method:'post',
      url:url, 
      data:data
    })
    .then(function(response){
     // console.log(response.data.message);
    })
  }
  

    render() {
      return (
          <div>

<form onSubmit={this.handleSubmit}>
       
       
            <label htmlFor="URL">URL:</label>
            <input
              id="URL"
              name="URL"
              type="text"
              required
            />
             <button>Submit</button>
      </form>
            
        
          </div>
      );
    }
  }

  export default Step2;