import React, {Component} from 'react';
import axios from 'axios';

class Step1 extends Component {
    constructor() {
      super();
      this.handleSubmit = this.handleSubmit.bind(this);
      this.changerandom = this.changerandom.bind(this);
      this.externalAPIcall = this.externalAPIcall.bind(this);
      this.externalPOSTcall = this.externalPOSTcall.bind(this);
    }
  
    handleSubmit(event) {
      event.preventDefault();

      const form = event.target;
      const data = new FormData(form);        
   //Function to stringify form data   
  function stringifyFormData(fd) {
    const data = {};
      for (let key of fd.keys()) {
        data[key] = fd.get(key);
    }
    return JSON.stringify(data, null, 2);
  }

  //Parsing stringified FormData
  var jsondata = JSON.parse(stringifyFormData(data))

      // console.log(jsondata.deviceID);
      this.externalAPIcall(jsondata.URL, jsondata.deviceID)
      this.changerandom(event)
    }

    changerandom(e){
      e.target.reset();
    }  
  
    externalAPIcall(url,deviceID){
      const username = "Neeraja"
      const password = "f7e5b1g4w4"
      const apikey = "264b63333e951a6c327d627003f6a828"
      var b = new Buffer(username + ':' + password);
      var encodeAuth = b.toString('base64')
      var urlstring = url //Assign the URL attribute here
      var loggerID = urlstring.slice(52, 60); //capturing substring from main string
      var pullID = loggerID + '.' + deviceID //merging dataloger ID with device ID

      fetch(urlstring, {
          headers: {
              'Authorization': 'Basic' + encodeAuth,
              'X-API-KEY' : apikey
          }
      })
      .then((response)=>{
        return response.json()
      })
      .then((responseJson) => {
        console.log(responseJson.data[pullID].P_AC)
        this.externalPOSTcall(responseJson.data[pullID].P_AC,loggerID,deviceID )
        // this.setState({
        //   data : responseJson.data["Id101409.1"].P_AC
        //   })
        })
  }

  externalPOSTcall(data, loggerID, deviceID){
    var dataloggerID;
    if(loggerID === "Id101409"){
      dataloggerID = 1;
    }
    if(loggerID === "Id101445"){
      dataloggerID = 2;
    }
    if(loggerID === "Id101408"){
      dataloggerID = 3;
    }
    if(loggerID === "Id101403"){
      dataloggerID = 4;
    }
    if(loggerID === "Id101338"){
      dataloggerID = 5;
    }
    if(loggerID === "Id101337"){
      dataloggerID = 6;
    }
    var url = "http://localhost:3003/P_AC31"
   
    var urlstring = url
    console.log(urlstring)
    var tempdata=data                        
    for(var i=0;i<tempdata.length;i++){
      tempdata[i].loggerID=dataloggerID;
    }
    console.log(tempdata);                                             
    axios({
      method:'post',
      url:url, 
      data:tempdata
    })
    .then(function(response){
      console.log(response.data.message);
    })
  }
  

    render() {
      return (
          <div>

<form onSubmit={this.handleSubmit}>
        <label> DeviceID :  </label>
          <select name="deviceID">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
            <option value="21">21</option>
            <option value="22">22</option>
            <option value="23">23</option>
            <option value="24">24</option>
            <option value="25">25</option>
            <option value="26">26</option>
            <option value="27">27</option>
            <option value="28">28</option>
            <option value="29">29</option>
            <option value="30">30</option>
            <option value="31">31</option>

          </select>
       
            <label htmlFor="URL">URL:</label>
            <input
              id="URL"
              name="URL"
              type="text"
              required
            />
             <button>Submit</button>
      </form>
            
        
          </div>
      );
    }
  }

  export default Step1;